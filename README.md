## Requirements

Java 11+  
Maven  
Netbeans 12 (optional)

## IDE instructions

- NetBeans: Open project -> select cloned folder
- Build/Run

## Command line instructions

```
mvn clean package
java -cp target/coxAssignment-1.0-SNAPSHOT.jar com.mycompany.coxassignment.Main
```