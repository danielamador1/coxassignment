package com.mycompany.coxassignment;
/**
 * @author Daniel
 */
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import com.google.gson.Gson;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        autoRetailRetriever regionalDataRetriever = new autoRetailRetriever();
        regionalDataRetriever.getDataFromDatasetId();
    }
}

class autoRetailRetriever{
    private HttpResponse<String> datasetResponse;
    private String datasetId = "";
    private VehiclesListDTO vehiclesList = new VehiclesListDTO();
    private VehicleIdListDTO vehicleListObj = new VehicleIdListDTO();
    private Set<Integer> dealerIds = Collections.synchronizedSet(new HashSet<>());
    private RegionalDealersDTO cityDealerInfo = new RegionalDealersDTO();
    private ArrayList<Thread> threads = new ArrayList<>();
    private final String DATASET_URL = "http://api.coxauto-interview.com/api/datasetId";

    private final HttpClient httpClient = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .build();
  
    private void clearValues(){
        datasetId = "";
        vehiclesList = new VehiclesListDTO();
        vehicleListObj = new VehicleIdListDTO();
        dealerIds = Collections.synchronizedSet(new HashSet<>());
        cityDealerInfo = new RegionalDealersDTO();
        threads = new ArrayList<>();
    }
  
    autoRetailRetriever(){
    }

    public void getDataFromDatasetId(){
        clearValues();
        datasetResponse = sendGet(DATASET_URL);
        if (datasetResponse != null){
            extractDatasetId();
            getVehiclesFromDatasetId();
            getVehicleInfoAsList();
            getDealerInfoAndGeneratePostObject();
            attachVehiclesToDealersListObject();
            sendPostDealersListData();
        }
    }
  
    private void extractDatasetId(){
        DatasetidDTO obj = new Gson().fromJson(datasetResponse.body(), DatasetidDTO.class);
        datasetId = obj.datasetId;
    }
  
    private void getVehiclesFromDatasetId(){
        HttpResponse<String> vehiclesResponse = sendGet(String.format("http://api.coxauto-interview.com/api/%s/vehicles", datasetId));
        vehicleListObj = new Gson().fromJson(vehiclesResponse.body(), VehicleIdListDTO.class);
    }
  
    private void getVehicleInfoAsList(){
        ListIterator<Integer> iterator = vehicleListObj.vehicleIds.listIterator();
        while (iterator.hasNext()) {
            createGetThread(String.format("http://api.coxauto-interview.com/api/%s/vehicles/%d", datasetId, iterator.next()));
        }
        waitForThreads();
    }
  
    private void getDealerInfoAndGeneratePostObject(){
        Iterator<Integer> dealerListIterator = dealerIds.iterator();
        while (dealerListIterator.hasNext()) {
            createGetThread(String.format( "http://api.coxauto-interview.com/api/%s/dealers/%d", datasetId, dealerListIterator.next()));
        }
        waitForThreads();
    }
  
    private void attachVehiclesToDealersListObject(){
        cityDealerInfo.dealers.forEach(dealer -> {
            dealer.vehicles = vehiclesList.vehicles.stream().filter(vehicle -> (vehicle.dealerId.equals(dealer.dealerId))).collect(Collectors.toList());
        });
    }
  
    private void sendPostDealersListData(){
        sendPost( String.format("http://api.coxauto-interview.com/api/%s/answer", datasetId) , cityDealerInfo);
    }
  
    private void createGetThread(String url){
        GetRequestRunnable newThread = new GetRequestRunnable(url);
        Thread t = new Thread(newThread);
        t.start();
        threads.add(t);
    }

    private void waitForThreads(){
        threads.forEach(t -> {
            try{
                t.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        threads = new ArrayList<>();
    }

    private HttpResponse sendGet(String url) {
        HttpRequest request = HttpRequest.newBuilder()
            .GET()
            .uri(URI.create(url))
            .build();
        try{
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return response;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendPost(String url, Object body) {
        HttpRequest request = HttpRequest.newBuilder()
            .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(body)))
            .uri(URI.create(url))
            .header("Content-Type", "application/json")
            .build();
        try{
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetRequestRunnable implements Runnable{
        private final String URL;
        private final int SUCCESS_STATUS_CODE = 200;

        GetRequestRunnable(String url){
            this.URL = url;
        }

        @Override
        public void run() {
            if (URL.contains("/vehicles/") ){
                getVehicles();
            } else if (URL.contains("/dealers/")) {
                getDealers();
            }
        }

        private void getVehicles(){
            HttpResponse<String> vehicleInfoResponse = sendGet(URL);
            if (vehicleInfoResponse.statusCode() == SUCCESS_STATUS_CODE){
                VehicleInfoDTO vehicleInfoObj = new Gson().fromJson(vehicleInfoResponse.body(), VehicleInfoDTO.class);
                vehiclesList.vehicles.add(vehicleInfoObj);
                dealerIds.add(vehicleInfoObj.dealerId);
            }
        }

        private void getDealers(){
            HttpResponse<String> dealerInfoResponse = sendGet(URL);
            if (dealerInfoResponse.statusCode() == SUCCESS_STATUS_CODE){
                DealerInfoDTO dealerInfoObj = new Gson().fromJson(dealerInfoResponse.body(), DealerInfoDTO.class);
                DealerAndAssetsListDTO tempObj = new DealerAndAssetsListDTO();
                tempObj.dealerId = dealerInfoObj.dealerId;
                tempObj.name = dealerInfoObj.name;
                cityDealerInfo.dealers.add(tempObj);
            }
        }
    }
}

class DatasetidDTO{
    String datasetId;
}

class VehicleIdListDTO{
    List<Integer> vehicleIds = new ArrayList<>();
}

class VehiclesListDTO{
    List<VehicleInfoDTO> vehicles = Collections.synchronizedList(new ArrayList<>());
}

class VehicleInfoDTO{
    Integer vehicleId;
    Integer year;
    String make;
    String model;
    Integer dealerId;
}

class DealersListDTO{
    List<DealerInfoDTO> dealers = new ArrayList<>();
}

class DealerInfoDTO{
    Integer dealerId;
    String name;
}

class DealerAndAssetsListDTO{
    Integer dealerId;
    String name;
    List<VehicleInfoDTO> vehicles = new ArrayList<>();
}

class RegionalDealersDTO{
    List<DealerAndAssetsListDTO> dealers = Collections.synchronizedList(new ArrayList<>());
}